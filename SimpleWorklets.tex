% -*- latex -*-

\chapter{Simple Worklets}
\label{chap:SimpleWorklets}

\index{worklet|(}
\index{worklet!creating|(}

The simplest way to implement an algorithm in \VTKm is to create a \keyterm{worklet}.
A worklet is fundamentally a functor that operates on an element of data.
Thus, it is a \textcode{class} or \textcode{struct} that has an overloaded parenthesis operator (which must be declared \textcode{const} for thread safety).
However, worklets are also embedded with a significant amount of metadata on how the data should be managed and how the execution should be structured.

\vtkmlisting[ex:SimpleWorklet]{A simple worklet.}{SimpleWorklet.cxx}

As can be seen in Example \ref{ex:SimpleWorklet}, a worklet is created by implementing a \textcode{class} or \textcode{struct} with the following features.

\begin{enumerate}
\item
  The class must publicly inherit from a base worklet class that specifies the type of operation being performed (line \ref{ex:SimpleWorklet.cxx:Inherit}).
\item
  The class must contain a functional type named \controlsignature (line \ref{ex:SimpleWorklet.cxx:ControlSignature}), which specifies what arguments are expected when invoking the class in the control environment.
\item
  The class must contain a functional type named \executionsignature (line \ref{ex:SimpleWorklet.cxx:ExecutionSignature}), which specifies how the data gets passed from the arguments in the control environment to the worklet running in the execution environment.
\item
  The class specifies an \inputdomain (line \ref{ex:SimpleWorklet.cxx:InputDomain}), which identifies which input parameter defines the input domain of the data.
%% \item
%%   The class may define a scatter operation to override a 1:1 mapping from input to output.
\item
  The class must contain an implementation of the parenthesis operator, which is the method that is executed in the execution environment (lines \ref{ex:SimpleWorklet.cxx:OperatorStart}--\ref{ex:SimpleWorklet.cxx:OperatorEnd}).
  The parenthesis operator must be declared \textcode{const}.
\end{enumerate}


\section{Control Signature}
\label{sec:ControlSignature}

\index{control signature|(}
\index{signature!control|(}
\index{worklet!control signature|(}

The control signature of a worklet is a functional type named \controlsignature.
The function prototype matches what data are provided when the worklet is invoked (as described in Section \ref{sec:InvokingWorklet}).

\vtkmlisting[ex:ControlSignature]{A \protect\controlsignature.}{ControlSignature.cxx}

\begin{didyouknow}
  If the code in Example~\ref{ex:ControlSignature} looks strange, you may be unfamiliar with \index{function types}function types.
  In C++, functions have types just as variables and classes do.
  A function with a prototype like

  \begin{quote}
    \textcode{void functionName(int arg1, float arg2);}
  \end{quote}

  has the type \textcode{void(int,float)}.
  \VTKm uses function types like this as a \index{signature}\emph{signature} that defines the structure of a function call.
\end{didyouknow}

The return type of the function prototype is always \textcode{void}.
The parameters of the function prototype are \index{signature tags}\keyterm{tags} that identify the type of data that is expected to be passed to invoke.
\controlsignature tags are defined by the worklet type and the various tags are documented more fully in Chapter \ref{chap:WorkletTypeReference}.
In the case of Example \ref{ex:ControlSignature}, the two tags \sigtag{FieldIn} and \sigtag{FieldOut} represent input and output data, respectively.

By convention, \controlsignature tag names start with the base concept (e.g. \textsignature{Field} or \textsignature{Topology}) followed by the domain (e.g. \textsignature{Point} or \textsignature{Cell}) followed by \textsignature{In} or \textsignature{Out}.
For example, \sigtag{FieldPointIn} would specify values for a field on the points of a mesh that are used as input (read only).
Although they should be there in most cases, some tag names might leave out the domain or in/out parts if they are obvious or ambiguous.

\index{worklet!control signature|)}
\index{signature!control|)}
\index{control signature|)}


\section{Execution Signature}
\label{sec:ExecutionSignature}

\index{execution signature|(}
\index{signature!execution|(}
\index{worklet!execution signature|(}

Like the control signature, the execution signature of a worklet is a functional type named \executionsignature.
The function prototype must match the parenthesis operator (described in Section \ref{sec:WorkletOperator}) in terms of arity and argument semantics.

\vtkmlisting{An \protect\executionsignature.}{ExecutionSignature.cxx}

The arguments of the \executionsignature's function prototype are tags that define where the data come from.
The most common tags are an underscore followed by a number, such as \sigtagnum{1}, \sigtagnum{2}, etc.
These numbers refer back to the corresponding argument in the \controlsignature.
For example, \sigtagnum{1} means data from the first control signature argument, \sigtagnum{2} means data from the second control signature argument, etc.

Unlike the control signature, the execution signature optionally can declare a return type if the parenthesis operator returns a value.
If this is the case, the return value should be one of the numeric tags (i.e. \sigtagnum{1}, \sigtagnum{2}, etc.)
to refer to one of the data structures of the control signature.
If the parenthesis operator does not return a value, then \executionsignature should declare the return type as \textcode{void}.

In addition to the numeric tags, there are other execution signature tags to represent other types of data.
For example, the \sigtag{WorkIndex} tag identifies the instance of the worklet invocation.
Each call to the worklet function will have a unique \sigtag{WorkIndex}.
Other such tags exist and are described in the following section on worklet types where appropriate.

\index{worklet!execution signature|)}
\index{signature!execution|)}
\index{execution signature|)}


\section{Input Domain}
\label{sec:InputDomain}

\index{input domain|(}
\index{worklet!input domain|(}

All worklets represent data parallel operations that are executed over independent elements in some domain.
The type of domain is inherent from the worklet type, but the size of the domain is dependent on the data being operated on.

A worklet identifies the argument specifying the domain with a type alias named \inputdomain.
The \inputdomain must be aliased to one of the execution signature numeric tags (i.e. \sigtagnum{1}, \sigtagnum{2}, etc.).
By default, the \inputdomain points to the first argument, but a worklet can override that to point to any argument.

\vtkmlisting{An \protect\inputdomain declaration.}{InputDomain.cxx}

Different types of worklets can have different types of domain.
For example a simple field map worklet has a \sigtag{FieldIn} argument as its input domain, and the size of the input domain is taken from the size of the associated field array.
Likewise, a worklet that maps topology has a \sigtag{CellSetIn} argument as its input domain, and the size of the input domain is taken from the cell set.

Specifying the \inputdomain is optional.
If it is not specified, the first argument is assumed to be the input domain.

\index{worklet!input domain|)}
\index{input domain|)}


\section{Worklet Operator}
\label{sec:WorkletOperator}

A worklet is fundamentally a functor that operates on an element of data.
Thus, the algorithm that the worklet represents is contained in or called from the parenthesis operator method.

\vtkmlisting{An overloaded parenthesis operator of a worklet.}{WorkletOperator.cxx}

There are some constraints on the parenthesis operator.
First, it must have the same arity as the \executionsignature, and the types of the parameters and return must be compatible.
Second, because it runs in the execution environment, it must be declared with the \vtkmexecmodifier (or \vtkmexeccontmodifier) modifier.
Third, the method must be declared \textcode{const} to help preserve thread safety.


\index{worklet!creating|)}


\section{Invoking a Worklet}
\label{sec:InvokingWorklet}

\index{worklet!invoke|(}

Previously in this chapter we discussed creating a simple worklet.
In this section we describe how to run the worklet in parallel.

A worklet is run using the \vtkmcont{Invoker} class.

\vtkmlisting[ex:WorkletInvoke]{Invoking a worklet.}{WorkletInvoke.cxx}

Using an \textidentifier{Invoker} is simple.
First, an \textidentifier{Invoker} can be simply constructed with no arguments (line \ref{ex:WorkletInvoke.cxx:Construct}).
Next, the \textidentifier{Invoker} is called as if it were a function (line \ref{ex:WorkletInvoke.cxx:Call}).

The first argument to the invoke is always an instance of the worklet.
The remaining arguments are data that are passed (indirectly) to the worklet.
Each of these arguments (after the worklet) match a corresponding argument listed in the \controlsignature.
So in the invocation on Example \ref{ex:WorkletInvoke}, line \ref{ex:WorkletInvoke.cxx:Call}, the second and third arguments correspond the the two \controlsignature arguments given in Example \ref{ex:ControlSignature}.
\textcode{psiArray} corresponds to the \textsignature{FieldIn} argument and \textcode{nmsArray} corresponds to the \textsignature{FieldOut} argument.

\index{worklet!invoke|)}


\section{Preview of More Complex Worklets}

This chapter demonstrates the creation of a worklet that performs a very simple math operation in parallel.
However, we have just scratched the surface of the kinds of algorithms that can be expressed with \VTKm worklets.
There are many more execution patterns and data handling constructs.
The following example gives a preview of some of the more advanced features of worklets.

\vtkmlisting{A more complex worklet.}{ComplexWorklet.cxx}

We will discuss the many features available in the worklet framework throughout Part \ref{part:Advanced}.


\index{worklet|)}
